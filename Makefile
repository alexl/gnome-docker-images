gnome-3-26:
	docker build --no-cache --build-arg RUNTIME_REMOTE=gnome --build-arg RUNTIME_VERSION=3.26 -t alexl/flatpak-builder-gnome:3.26 .

all: gnome-3-26
	true

deploy:
	docker push alexl/flatpak-builder-gnome:3.26
