FROM fedora:26
MAINTAINER [Alexander Larsson <alexl@redhat.com>]
RUN dnf -y install --enablerepo=updates-testing flatpak-builder ostree strace && dnf clean all
RUN mkdir /flatpak
ENV FLATPAK_USER_DIR=/flatpak
ENV FLATPAK_GL_DRIVERS=dummy
RUN flatpak --user remote-add gnome https://sdk.gnome.org/gnome.flatpakrepo
RUN flatpak --user remote-add gnome-nightly https://sdk.gnome.org/gnome-nightly.flatpakrepo
ARG RUNTIME_REMOTE=gnome-nightly
ARG RUNTIME_VERSION=master
RUN flatpak --user install gnome org.gnome.Sdk//${RUNTIME_VERSION} org.gnome.Platform//${RUNTIME_VERSION}
